# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=crun
pkgver=1.10
pkgrel=0
pkgdesc="Fast and lightweight fully featured OCI runtime and C library for running containers"
url="https://github.com/containers/crun"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
arch="all"
makedepends="libcap-dev libseccomp-dev yajl-dev argp-standalone python3 go-md2man"
subpackages="$pkgname-doc $pkgname-static"
source="https://github.com/containers/crun/releases/download/$pkgver/crun-$pkgver.tar.xz"

provides="oci-runtime"
provider_priority=100 # highest, default provider

# secfixes:
#   1.4.4-r0:
#     - CVE-2022-27650

build() {
	./configure \
		--prefix=/usr \
		--disable-systemd
	make
}

check() {
	make tests/tests_libcrun_errors.log
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
bf1e154869687f59eb3efd9eed6d37712842a8953088a340f3fb7c1d451db0dadb66eae7161c0ad6e2ec29749167cc1ff0815f722882b1fc95f7bee4f154e3f8  crun-1.10.tar.xz
"
