# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-tldextract
_pyname=tldextract
pkgver=5.0.0
pkgrel=0
pkgdesc="Accurately separate the TLD from the registered domain and subdomains of a URL"
url="https://github.com/john-kurkowski/tldextract"
arch="noarch"
license="BSD-3-Clause"
depends="
	python3
	py3-idna
	py3-requests
	py3-requests-file
	py3-filelock
	"
makedepends="py3-gpep517 py3-setuptools py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest py3-pytest-mock py3-responses"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/john-kurkowski/tldextract/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}
sha512sums="
6d6d70d7f0c230b199db46761ce784c49d0692d08e0bb9e9b880b8cfd1c248d674ba26f4172c338a9624d9f94bdb8d98a06ba73953e6ac1277e8526d6bd9bfd1  tldextract-5.0.0.tar.gz
"
